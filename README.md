# Sample Terraform


## Name
Sample terraform for AWS infrastructure

## Description
The purpose of this project is for showcasing terraform with AWS infrastructure

## Pre-requisite

- IAM user with the appropriate policy to create the resource with access key and secret key generated.
- Default VPC 
- Private Subnet and Public Subnet within the Default VPC


## Instructions:

1. Export aws secret key and access key using the following command

- export AWS_ACCESS_KEY_ID=""
- export AWS_SECRET_ACCESS_KEY=""

2. Create terraform.tfvars file and specify the following variable value

```
vpc_id = "" // default vpc id
private_subnet_id = "" // private subnet id
public_subnet_id = ""  // public subnet id
region="ap-southeast-1"
```

3. Run terraform init

4. Run terraform plan to check the resources that will be created

5. Run terraform apply to perform resource creation, type yes when asked.

## Result


1. Terraform apply result

![Terraform apply result](screenshot/screenshot-1.png)

2. SSH to public VM

![SSH to public vm](screenshot/screenshot-2.png)

3. SSH to private vm from public VM
![SSH to private vm from public vm](screenshot/screenshot-3.png)

4. Public VM web server default page
![Public vm web server default page](screenshot/screenshot-4.png)