variable "region" {}
variable "vpc_id" {}
variable "private_subnet_id" {}
variable "public_subnet_id" {}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}


data "aws_vpc" "default" {
  id = var.vpc_id
}

data "aws_subnet" "private_subnet" {
  id = var.private_subnet_id
}

data "aws_subnet" "public_subnet" {
  id = var.public_subnet_id
}

data "aws_ami" "amazon-2" {
  most_recent = true

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_security_group" "public" {
  name        = "public security group"
  description = "allow ssh and http to public security group"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "http public"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "ssh from public"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "public"
  }
}

resource "aws_network_interface" "public" {
  subnet_id       = data.aws_subnet.public_subnet.id
  security_groups = [aws_security_group.public.id]
}

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.public.id
  associate_with_private_ip = aws_network_interface.public.private_ip
}



resource "aws_instance" "public" {
  ami           = data.aws_ami.amazon-2.id  # us-west-2
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.public.id
    device_index         = 0
  }
  
  user_data = <<EOF

    #!/bin/bash

    yum update -y

    yum install httpd -y

    service httpd start

    chkconfig httpd on

    cd /var/www/html

    echo "<html><h1>Welcome To My Webpage</h1></html>" >

    index.html

  EOF

  credit_specification {
    cpu_credits = "unlimited"
  }

  key_name = aws_key_pair.kp.key_name

  tags = {
    Name = "public vm"
  }
}


resource "aws_security_group" "private" {
  name        = "private security group"
  description = "allow ssh from public security group"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "ssh from public"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${aws_network_interface.public.private_ip}/32"]
  }

  tags = {
    Name = "private"
  }
}

resource "aws_network_interface" "private" {
  subnet_id       = data.aws_subnet.private_subnet.id
  security_groups = [aws_security_group.private.id]
}


resource "aws_instance" "private" {
  ami           = data.aws_ami.amazon-2.id  # us-west-2
  instance_type = "t2.micro"
 
  network_interface {
    network_interface_id = aws_network_interface.private.id
    device_index         = 0
  }
  
  key_name = aws_key_pair.kp.key_name

  credit_specification {
    cpu_credits = "unlimited"
  }

  tags = {
    Name = "private vm"
  }
}


resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  key_name   = "myKey"       # Create "myKey" to AWS!!
  public_key = tls_private_key.pk.public_key_openssh

  provisioner "local-exec" {
   command = "echo '${tls_private_key.pk.private_key_pem}' >> mykey.pem"
  }
}

output "public_ipaddress" {
  value = aws_instance.public.public_ip
}

output "private_ipaddress" {
  value = aws_instance.private.private_ip
}